<?php

namespace App\Repositories;

use App\Interfaces\StudentRepositoryInterface;
use App\Models\Student;

class StudentRepository implements StudentRepositoryInterface
{
    public function getAllStudents()
    {
        return Student::all();
    }

    public function getStudentById($studentId)
    {
        return Student::findOrFail($studentId);
    }

    public function deleteStudent($studentId)
    {
        Student::destroy($studentId);
    }

    public function createStudent(array $studentDetails)
    {
        return Student::create($studentDetails);
    }
}
