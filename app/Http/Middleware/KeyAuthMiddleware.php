<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class KeyAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $key = $request->header('Authorization');

        if($key !== env('API_KEY')) {
            return response()->json([
                'error_mesage' => 'Unauthorized'
            ], 401);
        }

        return $next($request);
    }
}
