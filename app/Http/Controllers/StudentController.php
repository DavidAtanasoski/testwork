<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Interfaces\StudentRepositoryInterface;
use App\Models\Student;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psy\Util\Json;

class StudentController extends Controller
{
    private StudentRepositoryInterface $studentRepository;

    public function __construct(StudentRepositoryInterface $studentRepository, Student $student)
    {
        $this->studentRepository = $studentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() : JsonResponse
    {
        return response()->json([
            'data' => $this->studentRepository->getAllStudents()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStudentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudentRequest $request) : JsonResponse
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'year' => 'required'
        ]);

        $studentDetails = $request->only([
            'first_name',
            'last_name',
            'year'
        ]);

        return response()->json(
            [
                'data' => $this->studentRepository->createStudent($studentDetails)
            ],
            201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($studentId) : JsonResponse
    {
        return response()->json(
            [
                'data' => $this->studentRepository->getStudentById($studentId)
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStudentRequest  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentRequest $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($studentId) : JsonResponse
    {
        $this->studentRepository->deleteStudent($studentId);

        return response()->json(null);
    }

    /**
     * Search for a student
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function search($studentName) : JsonResponse
    {
        return response()->json([
           'data' => Student::where('first_name', 'like', $studentName)->get()
        ]);
    }

    /**
     * Filtering
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function filter() : JsonResponse
    {
        $students = Student::whereHas('courses', function ($query) {
            $query->where('course_name', 'Operating Systems');
        })->latest('students.last_name')->get();

        return response()->json([
            'data' => $students
        ]);
    }

    public function assignCourse(Request $request) : JsonResponse
    {
        $student = Student::find($request->input('student_id'));
        $student->courses()->attach($request->input('course_id'));

        return response()->json([
            'data' => $student
        ]);
    }

}
