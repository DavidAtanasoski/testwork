<?php

namespace App\Http\Controllers;

use App\Models\CourseOld;
use Illuminate\Http\Request;

class CourseControllerOld extends Controller
{
    public function addCourse(Request $request, CourseOld $course)
    {
        $course->course_name = $request->input('course_name');
        $course->course_code = $request->input('course_code');
        $course->save();

        return $course;
    }
}
