<?php

namespace App\Http\Controllers;

use App\Models\StudentOld;
use Illuminate\Http\Request;

class StudentControllerOld extends Controller
{
    public function addStudent(Request $request, StudentOld $student)
    {
        $student->first_name = $request->input('first_name');
        $student->last_name = $request->input('last_name');
        $student->year = $request->input('year');
        $student->save();

        return $student;
    }

    public function assignCourse(Request $request, StudentOld $student)
    {
        $student = StudentOld::find($request->input('student_id'));
        $student->courses()->attach($request->input('course_id'));

        return $student;
    }

    public function getStudentCourseInfo(Request $request, StudentOld $student)
    {
        $student = StudentOld::find($request->input('student_id'))
            ->courses()
            ->orderBy('course_name', 'desc')
            ->get();

        return $student;
    }
}
