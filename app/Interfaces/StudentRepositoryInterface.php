<?php

namespace App\Interfaces;

interface StudentRepositoryInterface
{
    public function getAllStudents();
    public function getStudentById($studentId);
    public function deleteStudent($orderId);
    public function createStudent(array $studentDetails);
}
