<?php

namespace App\Providers;

use App\Interfaces\CourseRepositoryInterface;
use App\Interfaces\StudentRepositoryInterface;
use App\Repositories\CourseRepository;
use App\Repositories\StudentRepository;
use Illuminate\Support\ServiceProvider;

class TestWorkServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->singleton(StudentOld::class, function ($app) {
//            return new StudentOld();
//        });
//
//        $this->app->singleton(CourseOld::class, function ($app) {
//            return new CourseOld();
//        });


        $this->app->bind(StudentRepositoryInterface::class, StudentRepository::class);
        $this->app->bind(CourseRepositoryInterface::class, CourseRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
