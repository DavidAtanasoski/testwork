<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseOld extends Model
{
    use HasFactory;

    protected $table = "courses";
    protected $fillable = [
        'course_name',
        'course_code'
    ];

    public function students()
    {
        return $this->belongsToMany(StudentOld::class, 'enrolment', 'course_id', 'student_id');
    }
}
