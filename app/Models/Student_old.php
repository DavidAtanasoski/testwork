<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentOld extends Model
{
    use HasFactory;

    protected $table = 'students';
    protected $fillable = [
        'first_name',
        'second_name',
        'year'
    ];

    public function courses()
    {
        return $this->belongsToMany(CourseOld::class, 'enrolment', 'student_id', 'course_id');
    }
}
