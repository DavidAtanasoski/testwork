<?php

//use App\Http\Controllers\StudentControllerOld;
//use App\Http\Controllers\CourseControllerOld;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "Welcome to TestWork!";
});

//Route::post('/addStudent', [StudentControllerOld::class, 'addStudent']);
//Route::post('/addCourse', [CourseControllerOld::class, 'addCourse']);
//Route::post('/assignCourse', [StudentControllerOld::class, 'assignCourse']);
//Route::post('/studentCourseInfo', [StudentControllerOld::class, 'getStudentCourseInfo'])->middleware('keyAuth');

