
## Test Work

Here is the prompted test work I got. What I've done and how I understood the assignment:
- I've created a MySQL database (called `testwork`) for tracking which student has enroled which course. The database consists of two many-to-many tables as requested. The one is called `users` and the other one is `courses`. Because of how these relations are modelled, there's also a pivot table called `enrolment`.
- For adding records to the database there are several endpoints:
  - `/addStudent` - where with a POST request we send a data which is JSON formatted.
  - `/addCourse` - does the same thing for the courses.
  - `/assignCourse` - with a POST request we send a `student_id` which is the ID of a student and `course_id` which is an ID of a course that the student have taken. These ID's are stored in the pivot table.
  - `/studentCourseInfo` - here we send a `student_id` and as a response we get the courses, in descending order by name,  that the student have enrolled. Also, this endpoint is secured with a middleware for authentication. I've created a `KeyAuthMiddleware` where a check if the `API_KEY` who is sent in the Header of the request is same as the one specified in the `.env` file. If it isn't I return a JSON object with a message.
  - In the project I've also created a Service Provider (`TestWorkServiceProvider`) which helps to have only one instance (using singleton) of models objects (Student and Course).

As this project is dockerized, you can install it using composer and it should run. The needed credentials are in the '.env' file.

**Disclaimer:** This is how I understood the assignment and if I'm missing something, please let me know.

